package conf

import "strings"

// splitConfigKey split config key to key and sub key
func splitConfigKey(key string) []string {
	key = strings.ToLower(key)
	key = strings.ReplaceAll(key, "/", ".")
	key = strings.ReplaceAll(key, "_", ".")
	return strings.Split(key, ".")
}
